//
//  SMStackTests.swift
//  SMStackTests
//
//  Created by Alexey Siginur on 19/10/2018.
//  Copyright © 2018 merkova. All rights reserved.
//

import XCTest
@testable import SMStack

class SMStackTests: XCTestCase {
	
	
	override static func setUp() {
		
	}

	
	override static func tearDown() {
	}

	
    func testStandart() {
		var stack = SMStack<Int>()
		stack.push(1)
		stack.push(2)
		stack.push(3)
		stack.push(4)
		
		XCTAssertEqual(stack[0].value, stack.head.value)
		XCTAssertEqual(stack[3].value, stack.tail.value)

		XCTAssertEqual(stack[0].value, 1)
		XCTAssertEqual(stack[1].value, 2)
		XCTAssertEqual(stack[2].value, 3)
		XCTAssertEqual(stack[3].value, 4)
		
		XCTAssertNil(stack[0].previous?.value)
		XCTAssertNil(stack[1].previous.previous?.value)
		XCTAssertNil(stack[2].previous.previous.previous?.value)
		XCTAssertNil(stack[3].previous.previous.previous.previous?.value)
		
		XCTAssertNil(stack[3].next?.value)
		XCTAssertNil(stack[2].next.next?.value)
		XCTAssertNil(stack[1].next.next.next?.value)
		XCTAssertNil(stack[0].next.next.next.next?.value)
		
		XCTAssertEqual(stack[0].value, 1)
		XCTAssertEqual(stack[0].next.value, 2)
		XCTAssertEqual(stack[0].next.next.value, 3)
		XCTAssertEqual(stack[0].next.next.next.value, 4)

		XCTAssertEqual(stack[1].previous.value, 1)
		XCTAssertEqual(stack[1].next.value, 3)
		XCTAssertEqual(stack[1].next.next.value, 4)
		
		XCTAssertEqual(stack[2].previous.previous.value, 1)
		XCTAssertEqual(stack[2].previous.value, 2)
		XCTAssertEqual(stack[2].next.value, 4)

		XCTAssertEqual(stack[3].previous.previous.previous.value, 1)
		XCTAssertEqual(stack[3].previous.previous.value, 2)
		XCTAssertEqual(stack[3].previous.value, 3)
		
		XCTAssertEqual(stack[0].next.previous.value, stack[0].value)
		XCTAssertEqual(stack.head.next.previous.value, stack.head.value)
		XCTAssertEqual(stack.tail.previous.next.value, stack.tail.value)

		XCTAssertNil(stack.head.previous?.value)
		XCTAssertEqual(stack.head.value, 1)
		XCTAssertEqual(stack.head.next.value, 2)
		XCTAssertEqual(stack.head.next.next.value, 3)
		XCTAssertEqual(stack.head.next.next.next.value, 4)
		XCTAssertNil(stack.head.next.next.next.next?.value)
		
		XCTAssertNil(stack.tail.next?.value)
		XCTAssertEqual(stack.tail.value, 4)
		XCTAssertEqual(stack.tail.previous.value, 3)
		XCTAssertEqual(stack.tail.previous.previous.value, 2)
		XCTAssertEqual(stack.tail.previous.previous.previous.value, 1)
		XCTAssertNil(stack.tail.previous.previous.previous.previous?.value)
		
		XCTAssertEqual(stack.pop(), 4)
		XCTAssertEqual(stack.pop(), 3)
		XCTAssertEqual(stack.pop(), 2)
		XCTAssertEqual(stack.pop(), 1)
		
		var i = 1
		for value in stack {
			XCTAssertEqual(value, i)
			i += 1
		}
    }
	
	
//	func asd() {
//
//	}
//
//
//    func testPerformanceExample() {
//        print("testPerformanceExample");
//        self.measure {
//            // Put the code you want to measure the time of here.
//        }
//    }

	
}
