//
//  StackIterator.swift
//  SMStack
//
//  Created by Alexey Siginur on 20/10/2018.
//  Copyright © 2018 merkova. All rights reserved.
//

public struct StackIterator<T>: IteratorProtocol {
	public typealias Element = T
	
	private var pointer: StackElementPointer<T>?
	
	init(_ stack: SMStack<T>) {
		self.pointer = stack._head
	}
	
	mutating public func next() -> T? {
		guard let pointer = self.pointer else {
			return nil
		}
		let value = pointer.pointee.value
		self.pointer = pointer.pointee.nextElement
		return value
	}
	
}
