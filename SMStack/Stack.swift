//
//  Stack.swift
//  SMStack
//
//  Created by Alexey Siginur on 19/10/2018.
//  Copyright © 2018 merkova. All rights reserved.
//

import Foundation

public struct SMStack<T> {
	
	internal var _head: StackElementPointer<T>?
	
	internal var _tail: StackElementPointer<T>?
	
	public var head: StackElement<T>! {
		get {
			return _head?.pointee;
		}
		set {
			_head?.initialize(to: newValue!)
		}
	}
	
	public var tail: StackElement<T>! {
		get {
			return _tail?.pointee;
		}
		set {
			_tail?.initialize(to: newValue!)
		}
	}
	
	public subscript (_ index: Int) -> StackElement<T>! {
		return get(index: index)
	}
	
	
	public init() {
		_head = nil
		_tail = nil
	}
	
	
	public init(value: T) {
		self.init()
		push(value)
	}
	
	
	public var isEmpty: Bool {
		return _head == nil
	}
	
	
	public var size: Int {
		var size = 0
		var pointer = head
		while pointer != nil {
			size += 1
			pointer = pointer!.next
		}
		return size
	}
	
	
	public func get(index: Int) -> StackElement<T>? {
		if let pointer = getPointer(atIndex: index) {
			return pointer.pointee
		}
		return nil
	}
	
	
	public mutating func insert(_ value: T, atIndex index: Int) {
		if index == 0 {
			let pointer = StackElementPointer<T>.create(value: StackElement(value: value, next: _head))
			_head?.pointee.setPrev(pointer)
			_head = pointer
		} else if let pointer = getPointer(atIndex: index - 1) {
			if pointer == _head! {
				_head = pointer.pointee.nextElement
			}
			pointer.pointee.insert(value)
		}
	}
	
	
	public mutating func insert(_ value: T, beforeIndex index: Int) {
		insert(value, atIndex: index - 1)
	}
	
	
	public mutating func insert(_ value: T, afterIndex index: Int) {
		insert(value, atIndex: index + 1)
	}
	
	
	public mutating func remove(atIndex index: Int) -> StackElement<T>? {
		if let pointer = getPointer(atIndex: index) {
			if pointer == _head! {
				_head = pointer.pointee.nextElement
			}
			if pointer == _tail! {
				_tail = pointer.pointee.previousElement
			}
			return pointer.pointee.remove()
		}
		return nil
	}
	
	
	public mutating func push(_ value: T) {
		let pointer = StackElementPointer<T>.create(value: StackElement(prev: _tail, value: value))
		if _head == nil || _tail == nil {
			_head = pointer
			_tail = pointer
		} else {
			_tail!.pointee.setNext(pointer)
			_tail = pointer
		}
	}
	
	
	@discardableResult
	public mutating func pop() -> T? {
		guard let _head = _head else {
			return nil
		}
		
		if _head == _tail {
			let result = _head.pointee.value
			_head.deallocate()
			self._head = nil
			self._tail = nil
			return result
		}
		
		guard let oldTail = _tail, let newTail = oldTail.pointee.previousElement else {
			return nil
		}
		let result = oldTail.pointee.value
		newTail.pointee.setNext(nil)
		self._tail!.deallocate()
		self._tail = newTail
		return result
	}
	
	
	public func peak() -> StackElement<T>? {
		return _tail?.pointee
	}
	
	
	private func getPointer(atIndex index: Int) -> StackElementPointer<T>? {
		var i = 0
		var pointer = _head
		while pointer != nil {
			if i == index {
				return pointer
			}
			i += 1
			pointer = pointer!.pointee.nextElement
		}
		return nil
	}
	
}

extension SMStack: Sequence {
	
	public func makeIterator() -> StackIterator<T> {
		return StackIterator(self)
	}
	
}
