//
//  StackElement.swift
//  SMStack
//
//  Created by Alexey Siginur on 19/10/2018.
//  Copyright © 2018 merkova. All rights reserved.
//

public struct StackElement<T> {
	private var _previous: StackElementPointer<T>?
	private var _next: StackElementPointer<T>?
	
	public var value: T
	
	public var next: StackElement<T>! {
		get {
			return _next?.pointee
		}
		set {
			_next?.initialize(to: newValue!)
		}
	}
	
	public var previous: StackElement<T>! {
		get {
			return _previous?.pointee
		}
		set {
			_previous?.initialize(to: newValue!)
		}
	}
	
}

internal extension StackElement {
	
	var nextElement: StackElementPointer<T>! {
		return _next
	}
	
	var previousElement: StackElementPointer<T>! {
		return _previous
	}
	
	init(prev: StackElementPointer<T>? = nil, value: T, next: StackElementPointer<T>? = nil) {
		self._previous = prev
		self.value = value
		self._next = next
	}
	
	mutating func setNext(_ element: StackElementPointer<T>?) {
		_next = element
	}
	
	mutating func setPrev(_ element: StackElementPointer<T>?) {
		_previous = element
	}
	
	mutating func insert(_ value: T) {
		let element = StackElement(prev: &self, value: value, next: _next)
		let pointer = StackElementPointer<T>.create(value: element)
		_next?.pointee.setPrev(pointer)
		setNext(pointer)
	}
	
	mutating func remove() -> StackElement<T>? {
		_next?.pointee._previous = _previous
		_previous?.pointee._next = _next
		let selfPointer: StackElementPointer<T> = UnsafeMutablePointer(&self)
		selfPointer.deinitialize(count: 1)
		return self
	}
	
}
