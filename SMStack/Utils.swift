//
//  Utils.swift
//  SMStack
//
//  Created by Alexey Siginur on 19/10/2018.
//  Copyright © 2018 merkova. All rights reserved.
//

internal typealias StackElementPointer<T> = UnsafeMutablePointer<StackElement<T>>

internal extension UnsafeMutablePointer {
	
	static func create<T>(value: StackElement<T>) -> UnsafeMutablePointer<StackElement<T>> {
		let pointer = UnsafeMutablePointer<StackElement<T>>.allocate(capacity: 1)
		pointer.initialize(to: value)
		return pointer
	}
	
}
